package org.blog;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.sequence.CommandVisitor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.text.WordUtils;

import java.lang.reflect.Array;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        User user = new User("Michael", 99);
        UserLombok userLombok = new UserLombok("Lombok",1);

        System.out.println(user.getAge());
        System.out.println(userLombok.getName());


//        Bag<String> cars = new HashBag<>();
//        cars.add("Audi", 3);
//        cars.add("Alfa Romeo",2);
//        cars.add("Fiat 126p");
//
//        cars.forEach(System.out::println);
//        System.out.println(cars.getCount("Audi"));
//
//        System.out.println(cars.size());
//        cars.uniqueSet().forEach(System.out::println);

//        String a = null;
//        System.out.println(StringUtils.defaultString(a));


//        String text = "przykładowy tekst";
//        String capitalized = StringUtils.capitalize(text);
//        String capitalized1 = WordUtils.capitalize(text);
//
//        System.out.println(capitalized);
//        System.out.println(capitalized1);


//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Jak masz na imie");
//        String name = scanner.nextLine();
//
//        String template = "Cześć ${name}!";
//        StringSubstitutor substitutor = new StringSubstitutor(Map.of("name", name));
//        String out = substitutor.replace(template);
//        System.out.println(out);




//        Map<String, String> map1 = new HashMap<>();
//        map1.put("Polska", "Warszawa");
//        map1.put("Niemcy", "Berlin");
//        map1.put("Francja", "Paryż");
//
//        Map<String, String> map2 = new HashMap<>();
//        map2.put("Polska", "Kraków");
//        map2.put("Czechy", "Praga");
//        map2.put("Francja", "Paryż");
//        map2.put("Słowacja", "Bratysława");
//        map2.put("Germany", "Berlin");
//
//        MapDifference<String, String> difference = Maps.difference(map1, map2);
//        Map<String, MapDifference.ValueDifference<String>> stringValueDifferenceMap = difference.entriesDiffering();
//        Map<String, String> stringStringMap = difference.entriesInCommon();
//        Map<String, String> stringStringMap1 = difference.entriesOnlyOnLeft();
//        Map<String, String> stringStringMap2 = difference.entriesOnlyOnRight();
//        System.out.println(stringStringMap2);
//        System.out.println(stringStringMap1);
//        System.out.println(stringStringMap);
//        System.out.println(stringValueDifferenceMap);

//        System.out.println(divide(2,0));

//        String text = ", Kamil,Mariusz,,Dominik ,";
//        String[] split = text.split(",");
//        List<String> names = Arrays.asList(split);
//        System.out.println(names);
//
//        List<String> strings = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(text);
//        System.out.println(strings);


//        List<String> names = new ArrayList<>();
//        names.add("Kamil");
//        names.add(null);
//        String joined1 = Joiner.on("; ").skipNulls().join(names);
//        String joined = String.join("; ", names);
//        String joined2 = Joiner.on(": ").useForNull("Domyślne imię").join(names);
//
//        System.out.println(joined);
//        System.out.println(joined1);
//        System.out.println(joined2);

    }
    private static int divide(int a, int b) {
        Preconditions.checkArgument(b != 0, "B 0!!!");
        return a/b;
    }
}